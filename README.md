## Minecraft Parameters Spoofer

Parameters spoofer for official Minecraft launcher. It can spoof (replace) parameters that launcher uses to launch the game.

It can bypass demo mode in official Minecraft launcher, so you can
play fully functional Minecraft without buying the game.

It also can change username to whatever you want.

---

## How to use

1. Download (or compile it yourself) spoofer executable for your OS
2. Open official Minecraft launcher and go to "Launch options"
3. Enable "Advanced settings" and open profile settings
4. Change "Java executable" path to spoofer executable path
5. Press "SAVE" button

After that spoofer will be used to launch the game.

---

## Settings

* To change username put desired name into **username.txt** file.
* To change path to Java executable put desired path into **java_path.txt** file.

Put these txt files into the game directory (.minecraft).